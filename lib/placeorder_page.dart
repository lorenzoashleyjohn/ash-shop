import 'package:flutter/material.dart';
import 'package:ashshop/confirm_page.dart';

class PlaceOrderPage extends StatelessWidget {
  const PlaceOrderPage({super.key});

  void _PlaceOrderPage(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const ConfirmPage()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Center(
          child: Column(
            children: [
              const SizedBox(height: 25),
              const Text(
                'Check Out',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 30,
                ),
              ),
              const SizedBox(height: 25),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  IconButton(
                    icon: Image.asset('lib/images/black.png'),
                    iconSize: 80,
                    onPressed: () {
                      (context);
                    },
                  ),
                  const SizedBox(width: 25),
                  Text(
                    'Black T Shirt \nP100.00',
                    style: TextStyle(
                      color: Colors.grey[700],
                      fontSize: 15,
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 15),
              const Divider(
                height: 15,
                thickness: 5,
                indent: 20,
                endIndent: 20,
                color: Colors.black,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Builder(builder: (context) {
                    return const SizedBox(width: 25);
                  }),
                  const Text(
                    'Delivery Address',
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const SizedBox(width: 25),
                  Text(
                    'Ashley John Lorenzo | 09324625758',
                    style: TextStyle(
                      color: Colors.grey[700],
                      fontSize: 15,
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const SizedBox(width: 25),
                  Text(
                    'Urdaneta City',
                    style: TextStyle(
                      color: Colors.grey[700],
                      fontSize: 15,
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const SizedBox(width: 25),
                  Text(
                    'Pangasinan, North Luzon',
                    style: TextStyle(
                      color: Colors.grey[700],
                      fontSize: 15,
                    ),
                  ),
                ],
              ),
              const Divider(
                height: 15,
                thickness: 5,
                indent: 20,
                endIndent: 20,
                color: Colors.black,
              ),
              const SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const SizedBox(width: 25),
                  const Text(
                    'Standard Local',
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  ),
                  const SizedBox(width: 150),
                  Text(
                    '60php',
                    style: TextStyle(
                      color: Colors.red[700],
                      fontSize: 20,
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const SizedBox(width: 25),
                  Text(
                    'Receive by June 6-8',
                    style: TextStyle(
                      color: Colors.grey[700],
                      fontSize: 15,
                    ),
                  ),
                ],
              ),
              const Divider(
                height: 15,
                thickness: 5,
                indent: 20,
                endIndent: 20,
                color: Colors.black,
              ),
              const SizedBox(height: 15),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Builder(builder: (context) {
                    return const SizedBox(width: 25);
                  }),
                  const Text(
                    'Order Total (3 item):',
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                  const SizedBox(width: 72),
                  const Text(
                    'P300.00',
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ],
              ),
              const Divider(
                height: 15,
                thickness: 5,
                indent: 20,
                endIndent: 20,
                color: Colors.black,
              ),
              const SizedBox(height: 25),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Builder(builder: (context) {
                    return const SizedBox(width: 25);
                  }),
                  const Text(
                    'Payment Option',
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                  const SizedBox(width: 75),
                  const Text(
                    'Cash on Delivery',
                    style: TextStyle(
                      fontSize: 17,
                    ),
                  ),
                ],
              ),
              const Divider(
                height: 15,
                thickness: 5,
                indent: 20,
                endIndent: 20,
                color: Colors.black,
              ),
              const SizedBox(height: 50),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Builder(builder: (context) {
                    return const SizedBox(width: 25);
                  }),
                  const Text(
                    'Payment Details',
                    style: TextStyle(
                      fontSize: 25,
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 15),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const SizedBox(width: 25),
                  Text(
                    'Merchandise Subtotal',
                    style: TextStyle(
                      color: Colors.grey[700],
                      fontSize: 20,
                    ),
                  ),
                  const SizedBox(width: 55),
                  Text(
                    'P300.00',
                    style: TextStyle(
                      color: Colors.grey[700],
                      fontSize: 20,
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const SizedBox(width: 25),
                  Text(
                    'Shipping Subtotal',
                    style: TextStyle(
                      color: Colors.grey[700],
                      fontSize: 20,
                    ),
                  ),
                  const SizedBox(width: 95),
                  Text(
                    '60php',
                    style: TextStyle(
                      color: Colors.grey[700],
                      fontSize: 20,
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 50),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Builder(builder: (context) {
                    return const SizedBox(width: 25);
                  }),
                  const Text(
                    'Total Payment',
                    style: TextStyle(
                      fontSize: 25,
                    ),
                  ),
                  const SizedBox(width: 90),
                  const Text(
                    'P360.00',
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 50),
              ElevatedButton(
                onPressed: () {
                  _PlaceOrderPage(context);
                },
                style: ElevatedButton.styleFrom(
                  primary: Colors.blue,
                  shape: const StadiumBorder(),
                  padding:
                      const EdgeInsets.symmetric(vertical: 16, horizontal: 150),
                ),
                child: const Text(
                  "Confirm",
                  style: TextStyle(fontSize: 20),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
