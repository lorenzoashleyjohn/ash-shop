import 'package:ashshop/home_page.dart';
import 'package:ashshop/placeorder_page.dart';

import 'package:flutter/material.dart';

class CartPage extends StatelessWidget {
  const CartPage({super.key});

  void _CartPage(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const PlaceOrderPage()),
    );
  }

  void CartPage1(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const HomePage()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Center(
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  IconButton(
                    icon: Image.asset('lib/images/backarrow.png'),
                    iconSize: 40,
                    onPressed: () {
                      CartPage1(context);
                    },
                  ),
                ],
              ),
              const SizedBox(height: 25),
              const Text(
                'Shopping Cart',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 50,
                ),
              ),
              const SizedBox(height: 25),
              Image.asset(
                'lib/images/addtocart.png',
                height: 170,
              ),
              const SizedBox(height: 350),
              ElevatedButton(
                onPressed: () {
                  _CartPage(context);
                },
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.blue,
                  shape: const StadiumBorder(),
                  padding:
                      const EdgeInsets.symmetric(vertical: 16, horizontal: 150),
                ),
                child: const Text(
                  "Confirm",
                  style: TextStyle(fontSize: 20),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
