import 'package:flutter/material.dart';
import 'package:ashshop/home_page.dart';

class ConfirmPage extends StatelessWidget {
  const ConfirmPage({super.key});

  void _ConfirmPage(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => HomePage()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Center(
          child: Column(
            children: [
              const SizedBox(height: 100),
              Text(
                'Thank You',
                style: TextStyle(
                  color: Colors.grey[700],
                  fontSize: 50,
                ),
              ),
              const SizedBox(height: 10),
              Text(
                'For Purchase!',
                style: TextStyle(
                  color: Colors.grey[700],
                  fontSize: 40,
                ),
              ),
              const SizedBox(height: 500),
              ElevatedButton(
                onPressed: () {
                  _ConfirmPage(context);
                },
                child: Text(
                  "Go Back",
                  style: TextStyle(fontSize: 20),
                ),
                style: ElevatedButton.styleFrom(
                  primary: Colors.blue,
                  shape: StadiumBorder(),
                  padding: EdgeInsets.symmetric(vertical: 16, horizontal: 100),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
