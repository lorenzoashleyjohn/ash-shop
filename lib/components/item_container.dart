import 'package:flutter/material.dart';

class ItemContainer extends StatelessWidget {
  final String imagePath;
  const ItemContainer({
    super.key,
    required this.imagePath,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(13),
        color: Colors.grey,
      ),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              imagePath,
              height: 110,
              width: 80,
            ),
          ],
        ),
      ),
    );
  }
}

class ItemContainer1 extends StatelessWidget {
  final String imagePath1;
  const ItemContainer1({
    super.key,
    required this.imagePath1,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(13),
        color: Colors.grey,
      ),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              imagePath1,
              height: 150,
              width: 105,
            ),
          ],
        ),
      ),
    );
  }
}
