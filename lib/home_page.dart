import 'package:ashshop/profile_page.dart';

import 'package:flutter/material.dart';
import 'package:ashshop/profile_page.dart';
import 'package:ashshop/cart_page.dart';
import 'package:ashshop/components/item_container.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  void _HomePage(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const CartPage()),
    );
  }

  void _HomePage2(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const CartPage()),
    );
  }

  void _HomePage3(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ProfilePage()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const SizedBox(height: 25),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    IconButton(
                      icon: Image.asset('lib/images/profile.png'),
                      iconSize: 40,
                      onPressed: () {
                        _HomePage3(context);
                      },
                    ),
                    const SizedBox(width: 240),
                    IconButton(
                      icon: Image.asset('lib/images/cart.png'),
                      iconSize: 40,
                      onPressed: () {
                        _HomePage2(context);
                      },
                    ),
                  ],
                ),
                const SizedBox(height: 25),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const SizedBox(width: 25),
                    const ItemContainer(imagePath: 'lib/images/black.png'),
                    const SizedBox(width: 15),
                    const Text('Black T Shirt\n100php'),
                    const SizedBox(width: 20),
                    ElevatedButton(
                      onPressed: () {
                        _HomePage(context);
                      },
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.blue,
                        shape: const StadiumBorder(),
                        padding: const EdgeInsets.symmetric(
                            vertical: 10, horizontal: 15),
                      ),
                      child: const Text(
                        'Add to cart',
                        style: TextStyle(fontSize: 15),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const SizedBox(width: 25),
                    const ItemContainer(imagePath: 'lib/images/blue.png'),
                    const SizedBox(width: 15),
                    const Text('Blue T Shirt\n100php'),
                    const SizedBox(width: 25),
                    ElevatedButton(
                      onPressed: () {
                        _HomePage(context);
                      },
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.blue,
                        shape: const StadiumBorder(),
                        padding: const EdgeInsets.symmetric(
                            vertical: 10, horizontal: 15),
                      ),
                      child: const Text(
                        'Add to cart',
                        style: TextStyle(fontSize: 15),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const SizedBox(width: 25),
                    const ItemContainer(imagePath: 'lib/images/red.png'),
                    const SizedBox(width: 15),
                    const Text('Red T Shirt\n100php'),
                    const SizedBox(width: 30),
                    ElevatedButton(
                      onPressed: () {
                        _HomePage(context);
                      },
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.blue,
                        shape: const StadiumBorder(),
                        padding: const EdgeInsets.symmetric(
                            vertical: 10, horizontal: 15),
                      ),
                      child: const Text(
                        'Add to cart',
                        style: TextStyle(fontSize: 15),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 25),
                const Divider(
                  height: 15,
                  thickness: 5,
                  indent: 20,
                  endIndent: 20,
                  color: Colors.black,
                ),
                const SizedBox(height: 25),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const SizedBox(width: 25),
                    const ItemContainer1(imagePath1: 'lib/images/blue.png'),
                    const SizedBox(width: 15),
                    const Text('Blue T Shirt\n100php'),
                    const SizedBox(width: 25),
                    ElevatedButton(
                      onPressed: () {
                        _HomePage(context);
                      },
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.blue,
                        shape: const StadiumBorder(),
                        padding: const EdgeInsets.symmetric(
                            vertical: 10, horizontal: 15),
                      ),
                      child: const Text(
                        'Add to cart',
                        style: TextStyle(fontSize: 15),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
