// ignore_for_file: sort_child_properties_last, deprecated_member_use, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:ashshop/login_page.dart';

class WelcomePage extends StatelessWidget {
  const WelcomePage({super.key});

  void _WelcomePage(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Center(
          child: Column(
            children: [
              const SizedBox(height: 80),
              Text(
                'Welcome',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 50,
                ),
              ),
              const SizedBox(height: 25),
              Text(
                'AshShop',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 30,
                ),
              ),
              const SizedBox(height: 45),
              Image.asset(
                'lib/images/logo.png',
                height: 200,
              ),
              const SizedBox(height: 200),
              ElevatedButton(
                onPressed: () {
                  _WelcomePage(context);
                },
                child: const Text(
                  "Next",
                  style: TextStyle(fontSize: 20),
                ),
                style: ElevatedButton.styleFrom(
                  primary: Color.fromARGB(255, 62, 205, 249),
                  shape: StadiumBorder(),
                  padding: EdgeInsets.symmetric(vertical: 16, horizontal: 100),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
